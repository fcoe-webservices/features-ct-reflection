<?php
/**
 * @file
 * features_ct_reflection.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function features_ct_reflection_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function features_ct_reflection_node_info() {
  $items = array(
    'reflection' => array(
      'name' => t('Reflection'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Use the following list to answer the survey questions.
<ul>
<li>0 = No Experience</li>
<li>1 = Directed Toward</li>
<li>2 = Approaches</li>
<li>3 = Meets</li>
<li>4 = Exemplifies</li>
</ul>
<ul class="hidden">
<li>0 = No Experience</li>
<li>1 = Directed Toward</li>
<li>2 = Approaches</li>
<li>3 = Meets</li>
<li>4 = Exemplifies</li>
</ul>'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
