<?php
/**
 * @file
 * features_ct_reflection.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function features_ct_reflection_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_standard_1_items|node|reflection|form';
  $field_group->group_name = 'group_standard_1_items';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'reflection';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Standard 1',
    'weight' => '2',
    'children' => array(
      0 => 'field_fc_standard_1_item_1',
      1 => 'field_fc_standard_1_item_2',
      2 => 'field_fc_standard_1_item_3',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Standard 1',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'standard-group',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_standard_1_items|node|reflection|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_standard_2|node|reflection|form';
  $field_group->group_name = 'group_standard_2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'reflection';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Standard 2',
    'weight' => '3',
    'children' => array(
      0 => 'field_fc_standard_2_item_1',
      1 => 'field_fc_standard_2_item_2',
      2 => 'field_fc_standard_2_item_3',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Standard 2',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'standard-group',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_standard_2|node|reflection|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_standard_3|node|reflection|form';
  $field_group->group_name = 'group_standard_3';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'reflection';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Standard 3',
    'weight' => '4',
    'children' => array(
      0 => 'field_fc_standard_3_item_1',
      1 => 'field_fc_standard_3_item_2',
      2 => 'field_fc_standard_3_item_3',
      3 => 'field_fc_standard_3_item_4',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Standard 3',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'standard-group',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_standard_3|node|reflection|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_standard_4|node|reflection|form';
  $field_group->group_name = 'group_standard_4';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'reflection';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Standard 4',
    'weight' => '5',
    'children' => array(
      0 => 'field_fc_standard_4_item_1',
      1 => 'field_fc_standard_4_item_2',
      2 => 'field_fc_standard_4_item_3',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Standard 4',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'standard-group',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_standard_4|node|reflection|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_standard_5|node|reflection|form';
  $field_group->group_name = 'group_standard_5';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'reflection';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Standard 5',
    'weight' => '6',
    'children' => array(
      0 => 'field_fc_standard_5_item_1',
      1 => 'field_fc_standard_5_item_2',
      2 => 'field_fc_standard_5_item_3',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Standard 5',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'standard-group',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_standard_5|node|reflection|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_standard_6|node|reflection|form';
  $field_group->group_name = 'group_standard_6';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'reflection';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Standard 6',
    'weight' => '7',
    'children' => array(
      0 => 'field_fc_standard_6_item_1',
      1 => 'field_fc_standard_6_item_2',
      2 => 'field_fc_standard_6_item_3',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Standard 6',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'standard-group',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_standard_6|node|reflection|form'] = $field_group;

  return $export;
}
